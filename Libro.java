public class Libro {

    String titulo;
    String autor;
    long isbn;
    Integer nroDePaginas;

   public void setTitulo(String titulo){
       this.titulo = titulo;
   }

   public void setAutor(String autor){
       this.autor = autor;
   }

   public void setISBN(long isbn){
       this.isbn = isbn;
   }

   public void setNroDePaginas(Integer nroDePaginas){
       this.nroDePaginas = nroDePaginas;
   }

   public String getTitulo(){
       return titulo;
   }

   public String getAutor(){
       return autor;
   }

   public long getISBN(){
       return isbn;
   }

   public Integer getNroDePaginas(){
       return nroDePaginas;
   }

   public void informacionDelLibro(){
       System.out.println("\nEl libro con ISBN "+isbn+", creado por el autor: "+autor+", tiene "+nroDePaginas+" paginas.");
       System.out.println("______________");
   }

}