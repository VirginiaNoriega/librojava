public class Ejecutable {
    public static void main(String[] args) {
    
        Libro libro1 = new Libro();
        Libro libro2 = new Libro();
    
        libro1.setTitulo ("Harry Potter y la piedra filosofal");
        libro1.setAutor ("Joanne Rowling");
        libro1.setISBN (978847888);
        libro1.setNroDePaginas (309);
    
        libro2.setTitulo ("El Senor de los Anillos, La Comunidad del Anillo");
        libro2.setAutor ("J. R. R. Tolkien");
        libro2.setISBN (787854321);
        libro2.setNroDePaginas (568);

        libro1.informacionDelLibro();
        libro2.informacionDelLibro();
    
    }
        

}